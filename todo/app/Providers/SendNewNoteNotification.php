<?php

namespace App\Providers;

use App\Http\Controllers\TasksController;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNewNoteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Http\Controllers\TasksController  $event
     * @return void
     */
    public function handle(TasksController $event)
    {
        //
    }
}
